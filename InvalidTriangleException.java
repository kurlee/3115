
public class InvalidTriangleException extends Exception{
    private String message;


    InvalidTriangleException(String message){
        super(message);
        this.message = message;
    }

    //currently unused constructor with cause
    InvalidTriangleException(String message, Throwable e){
        super(message, e);

    }

}
