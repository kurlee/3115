/* This circle class extends Geometric Objects, and is extended by Cylinder
From Geometric Objects, circles can have colors and are counted within the total number of shapes

Note: because Cylinder had a circle object as a member, a Cylinder will count as two shapes, a cylinder, and a circle.

TODO

 */



public class Circle extends GeometricObject implements Comparable<GeometricObject>{
    private double radius = 1.0;
    private static int numberOfCircles = 0;
    private int count;

    // No-arg Constructor
    Circle(){
        numberOfCircles++;
        count++;
    }

    Circle(double radius){
        this.radius =  radius;
        numberOfCircles++;
        count++;

    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public double getArea(){
        return radius*radius*Math.PI;
    }

    public double getDiameter(){
        return radius*2;

    }
    @Override
    public double getPerimeter(){
        return 2*Math.PI*radius;
    }

    public static int getNumberOfCircles(){
        return numberOfCircles;
    }

    @Override
    public int getCount(){
        return count;
    }

}
