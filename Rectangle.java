public class Rectangle extends GeometricObject implements Comparable<GeometricObject> {
    private double side1 = 1;
    private double side2 = 1;
    private int count;
    private static int numberOfSquares = 0;

    Rectangle(){
        numberOfSquares++;
        count = numberOfSquares;
    }


    Rectangle(double side1, double side2){
        this.side1 = side1;
        this.side2 = side2;
        numberOfSquares++;
        count = numberOfSquares;
    }

    public void setSide1(double side1){
        this.side1 = side1;
    }

    public double getSide1(){
        return side1;
    }

    public void setSide2(double side2){
        this.side2 = side2;
    }

    public double getSide2(){
        return side2;
    }

    @Override
    public double getPerimeter(){
        return(side1*2)+(side2*2);
    }

    @Override
    public double getArea(){
        return (side1*side2);
    }


    public static int getNumberOfSquares(){
        return numberOfSquares;
    }

    @Override
    public int getCount(){
        return count;
    }
}
