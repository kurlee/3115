import java.util.*;
/* This class extends Geometric Objects. It creates triangular objects



TODO
add the ability to calculate angles based on the measurements of all sides
other trig functionality would be fun as well
 */
class Triangle extends GeometricObject implements Comparable<GeometricObject>{
    //data field for triangle
    private double base = 1;
    private double side1 = 1;
    private double side2 = 1;
    private static int numberOfTriangles = 0;
    private int count;

    //No-Arg Constructor
    Triangle() throws InvalidTriangleException{
        numberOfTriangles++;
        triangleValidation();
        count = numberOfTriangles;
    }


    Triangle(double base, double side1, double side2)throws InvalidTriangleException{
        this.base = base;
        this.side1 = side1;
        this.side2 = side2;
        triangleValidation();
        numberOfTriangles++;
        count = numberOfTriangles;
    }


    //This constructor is for triangles where all sides are known. This triangle will also inherit a height, but it should not be used. Changes the objects color and fill as well.
    Triangle (double base, double side1, double side2, String color, Boolean filled) throws InvalidTriangleException {
        super(color, filled);
        setFilled(filled);
        this.base = base;
        this.side1 = side1;
        this.side2 = side2;
        triangleValidation();
        numberOfTriangles++;
        count = numberOfTriangles;

    }

    //Checks if triangle meets rules regarding the sides of triangles and throws exception if not.
    public void triangleValidation()throws InvalidTriangleException{
        if (base+side1 < side2 || base+side2 < side1 || side1+side2 < base || base < 0 || side1 < 0 || side2 < 0){
            //I only have a basic Exception constructor that only accepts a message at this time.
            throw new InvalidTriangleException("Invalid Triangle Exception: This triangle has illegal side lengths");
        }

    }

    public static int getNumberOfTriangles() {
        return numberOfTriangles;
    }

    @Override
    public int getCount() {
        return count;
    }
    // This set method is specifically to change only one of the sides of the triangle object.
    // It will check if the triangle is valid after changing the value and throw an exception if not
    public void setBase(double base)throws InvalidTriangleException{
        try {
            this.base = base;
            triangleValidation();
        }
        catch(InvalidTriangleException ex){
            System.out.println(ex.getMessage());
            resolveInvalidTriangle();
        }
    }

    //Returns current value for base
    public double getBase() {
        return base;
    }


    // This set method is specifically to change only one of the sides of the triangle object.
    // It will check if the triangle is valid after changing the value and throw an exception if not
    public void setSide1(double side1) throws InvalidTriangleException{
        try {
            this.side1 = side1;
            triangleValidation();
        }
        catch(InvalidTriangleException ex){
            System.out.println(ex.getMessage());
            resolveInvalidTriangle();
        }
    }

    public double getSide1(){
        return side1;
    }


    // This set method is specifically to change only one of the sides of the triangle object.
    // It will check if the triangle is valid after changing the value and throw an exception if not
    public void setSide2(double side2) throws InvalidTriangleException{
        try {
            this.side2 = side2;
            triangleValidation();
        }
        catch(InvalidTriangleException ex){
            System.out.println(ex.getMessage());
            resolveInvalidTriangle();
        }
    }

    public double getSide2() {
        return side2;
    }


    /*This method is an attempt to recover from an invalid triangle exception.
    The exception is thrown if
    1. A triangle with illegal side lengths is constructed
    2. A triangle with valid side lengths, has one of the lengths changed using a set method and it fails the validation test
    This method allows the recovery from this exception by giving the user a chance to input the values of each side
    If the values, once again, fail validation, the program exits with code 1
     */
    public void resolveInvalidTriangle()throws InvalidTriangleException{
        System.out.println("Any two sides of a triangle must add to greater than the remaining side");
        Scanner scanner = new Scanner(System.in);
        System.out.print("Lets attempt to recover from this error, re-enter the sides of this triangle\nBase: ");
        this.base = scanner.nextDouble();
        System.out.print("Side One: ");
        this.side1 = scanner.nextDouble();
        System.out.print("Side Two: ");
        this.side2 = scanner.nextDouble();
        try{
            triangleValidation();
        }
        catch(InvalidTriangleException ex2){
            System.out.println(ex2.getMessage());
            System.out.println("Exiting");
            System.exit(1);
        }
    }

    public boolean isEquilateral(){
        return side1 == side2 && side2 == base;
    }

    public boolean isIsosceles(){
        return (base==side1 ^ side1==side2 ^ side2==base ) ^ ( base==side1 && side1==side2 && side2==base );
    }

    public boolean isScalene(){
        return base != side1 && base != side2 && side1 != side2;
    }

    // this method identifies if the triangle is a right triangle by determining the longest side. If the triangle is Right, then this will be the hypotenuse.
    //then it uses pythagorean theorem to determine if each of the sides is the right length to make this a Right triangle.

    public boolean isRight() {
        if (base > side1 && base > side1)
            return (base * base) == (side1 * side1) + (side2 * side2);

        if (side1 > base && side1 > side2)
            return (side1 * side1) == (base * base) + (side2 * side2);

        if (side2 > side1 && side2 > base)
            return (side2 * side2) == (side1 * side1) + (base * base);
        return false;
    }

     @Override
    public double getPerimeter(){
        return base+side1+side2;
    }

    //Returns area using heron's formula
    @Override
    public double getArea() {
        double p = getPerimeter()/2.0;
        return Math.sqrt(p*(p-side1)*(p-side2)*(p-base));
    }

    public double getHeight(){
        return (2*getArea())/base;
    }

    public void ASCIIPrint(){
        System.out.println();
        System.out.println("Side 1: " + getSide1());
        if(isFilled()){
            System.out.println("              /\\");
            System.out.println("             /||\\");
            System.out.println("            /||||\\");
            System.out.println("           /||||||\\");
            System.out.println("          /||||||||\\  Side 2: " + getSide2());
            System.out.println("         /||||||||||\\");
            System.out.println("        /||||||||||||\\");
            System.out.println("       /||||||||||||||\\");
            System.out.println("      /||||||||||||||||\\");
            System.out.println("     /||||||||||||||||||\\");
            System.out.println("    /____________________\\");
        }
        else {
            System.out.println("              /\\");
            System.out.println("             /  \\");
            System.out.println("            /    \\");
            System.out.println("           /      \\");
            System.out.println("          /        \\  Side 2: " + getSide2());
            System.out.println("         /          \\");
            System.out.println("        /            \\");
            System.out.println("       /              \\");
            System.out.println("      /                \\");
            System.out.println("     /                  \\");
            System.out.println("    /____________________\\");
        }
        System.out.println("    Base: " + getBase());
    }
}
