/* This class is extended by all other classes except the test class. The purpose of this class is to give
shapes characteristics that all shapes can have, such as color, and time of creation"

TODO
 */


import java.util.Date;

public abstract class GeometricObject implements Comparable<GeometricObject>{
    private String color = "none";
    private boolean filled;
    private java.util.Date dateCreated = new Date();
    private static int numberOfShapes = 0;

    GeometricObject(){
        numberOfShapes++;
    }

    GeometricObject(String color, Boolean filled){
        numberOfShapes++;
        this.color = color;
        this.filled = filled;

    }

    public boolean isFilled() {
        return filled;
    }


    public void setColor(String color) {
        this.color = color;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public String getColor() {
        return color;
    }

    public static int getNumberOfShapes() {
        return numberOfShapes;
    }

    @Override
    public String toString() {
        if (filled){
            return "created on "+dateCreated+",  filled with the color "+color;
        }
        return "created on "+dateCreated+",  color "+color+", not filled in.";
    }

    @Override
    //Geometric Object implements Comparable for two reasons.  Firstly it is abstract and declares an abstract getArea method
    //this is helpful because we know that all subclasses will have a getArea method and that ensures that they are comparable.
    //Secondly, all shapes that extend geometric object will be comparable in the same way making it vague. This also makes
    //it possible to compare shapes of different types.
    public int compareTo(GeometricObject o) {
        if (getArea() > o.getArea())
            return 1;
        if (getArea() < o.getArea())
            return -1;
        return 0;
    }

    public abstract int getCount();


    public abstract double getPerimeter();


    public abstract double getArea();


}
