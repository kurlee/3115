/*
TODO

 */

public class TestTriangle {
    public static void main(String[] args) throws InvalidTriangleException {
        Triangle t1 = new Triangle(2, 4, 5);
        Triangle t2 = new Triangle(2, 4, 5);


        printComparison(t1, t2);
        t2.setSide2(3);
        printComparison(t1, t2);
        t2.setBase(7);
        t2.setSide1(9);
        printComparison(t1, t2);



        Rectangle r1 = new Rectangle(45, 45);
        Rectangle r2 = new Rectangle(45, 45);

        printComparison(r1, r2);
        r2.setSide1(88);
        printComparison(r1, r2);
        r2 = new Rectangle();
        printComparison(r1, r2);

        Circle c1 = new Circle(3.6);
        Circle c2 = new Circle(3.6);

        printComparison(c1, c2);
        c2.setRadius(4);
        printComparison(c1, c2);
        c2.setRadius(2);
        printComparison(c1, c2);


        printComparison(t1, r1);
        printComparison(c1, r1);
        printComparison(t2, c2);

    }

    public static void printComparison(GeometricObject o1, GeometricObject o2) {
        String s = "";
        if (o1.getClass() == o2.getClass()) {
            System.out.println("Comparing two objects of " + o1.getClass());
            switch (o1.compareTo(o2)) {
                case 0:
                    s = "Neither ";
                    break;
                case 1:
                    s = "The first ";
                    break;
                case -1:
                    s = "The Second ";
                    break;
            }
            System.out.println(s + o1.getClass() + " has the greater area\n");


        }

        if (o1.getClass() != o2.getClass()) {
            System.out.println("Comparing a " + o1.getClass() + ", and a " + o2.getClass());

            switch (o1.compareTo(o2)) {
                case 0:
                    s = "Both shapes have the same area";
                    break;
                case 1:
                    s = "The " + o1.getClass();
                    break;
                case -1:
                    s = "The " + o2.getClass();
                    break;
            }
            System.out.println(s +  " has the greater area\n");
        }
        System.out.println("___________________________________________");
    }
}
