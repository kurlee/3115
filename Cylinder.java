/* First of the 3D shapes.  Cylinder creates a circle object for every cylinder to use as the circular face.
This class only gives that circle height and calculates the respective characteristics of a cylinder.
This object can have weight which is used for a density calculation
 */
public class Cylinder extends GeometricObject{
    private double height = 1.0;
    private double weight = 1.0;
    Circle circularFace = new Circle();
    private static int numberOfCylinders = 0;
    public int count;

    Cylinder(){
        numberOfCylinders++;
        count++;

    }

    // This constructor allows user to create a cylinder of any dimension and updates the related circle as well.
    Cylinder(double height, double radius){
        circularFace.setRadius(radius);
        this.height = height;
        numberOfCylinders++;
        count++;

    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public double getVolume(){
        return circularFace.getArea()*height;

    }


    @Override
    public double getPerimeter() {
        return 2*(Math.PI*circularFace.getDiameter()*height);
    }
    @Override
    public double getArea() {
        return (2* Math.PI*circularFace.getRadius()*height)+(2* Math.PI*(circularFace.getRadius()*circularFace.getRadius()));
    }

    public static int getNumberOfCylinders(){
        return numberOfCylinders;
    }
    @Override
    public int getCount() {
        return count;
    }
}
